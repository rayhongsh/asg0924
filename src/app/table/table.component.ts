import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {

  constructor(private httpService: HttpClient) { }

  // rows and columns of table
  tbRows: string[];
  tbCols: string[];

  /**
   * page index related:
   * currPage: index of current page
   * totalPage: total number of pages
   * startPage: index of first page displayed in pagination
   * endPage: index of last page displayed in pagination
   */
  currPage = 1;
  totalPage: number;
  startPage: number;
  endPage: number;

  /**
   * page size related
   * sizeList: selections of page size
   * rowsEachPage: number of rows displayed in each page
   */
  sizeList: Array<number> = [20, 40, 60];
  rowsEachPage = this.sizeList[0];

  // allItems: array holding all data
  allItems: any[];

  // displayPageNumbers: array holding page index used by pagination
  displayPageNumbers: Array<number> = [1, 2, 3, 4, 5];

  ngOnInit() {

    // import data from assets file
    this.httpService.get('./assets/sample_data.json')
      .subscribe( data => {
        // data input, initialize total page number
        this.allItems = data as string [];
        this.changePage(this.currPage);
        this.totalPage = Math.ceil( this.allItems.length / this.rowsEachPage);
        this.createPageButtons();
      },
      (err: HttpErrorResponse) => {
        console.log (err.message);
      }
    );
  }

  /**
   * showPage() show page based on current page index and the amount of data each
   * page should display
   */
  showPage(currPageIndex: number, itemsEachPage) {
    // calculate start and end index of row in current page
    const inputTotalLength = this.allItems.length;
    const startIndex = (currPageIndex - 1) * itemsEachPage;
    const endIndex = Math.min(currPageIndex * itemsEachPage, inputTotalLength) - 1;

    // collection of data
    this.tbRows = this.allItems.slice(startIndex , endIndex + 1);
    // fields of data
    this.tbCols = Object.keys(this.tbRows[0]);
  }

  /**
   * changeItemEachPage() when user changes the amount of item displayed each page,
   * reset current displayed page index to 1 and update the data displayed in table
   */
  changeItemsEachPage() {
    this.totalPage = Math.ceil( this.allItems.length / this.rowsEachPage);
    this.showPage(1, this.rowsEachPage);
    this.changePage(1);
  }

  /**
   * changePage: take a page index as input and display the required page
   */
  changePage(pageNumber: number) {
    this.currPage = pageNumber;
    this.showPage(pageNumber, this.rowsEachPage);
    this.createPageButtons();
  }

  /**
   * prevPage() displays previous page
   */
  prevPage(pageNumber: number) {
    if (pageNumber === 1 ) {
      return;
    }
    this.currPage--;
    this.changePage(this.currPage);
  }

  /**
   * nextPage() displays next page
   */
  nextPage(pageNumber: number) {
    if (pageNumber === this.totalPage ) {
      return;
    }
    this.currPage++;
    this.changePage(this.currPage);
  }

  /**
   * pageButtons() create page index buttons for pagination
   * only display valid page index and 5 page indexs at most.
   */
  createPageButtons() {
    if (this.totalPage <= 5) {
      this.startPage = 1;
      this.endPage = this.totalPage;
    } else {
      if (this.currPage <= 3) {
        this.startPage = 1;
        this.endPage = 5;
      } else if (this.currPage + 1 >= this.totalPage) {
        this.startPage = this.totalPage - 4;
        this.endPage = this.totalPage;
      } else {
        this.startPage = this.currPage - 2;
        this.endPage = this.currPage + 2;
      }
    }

    // output an array of indexs to be used by page number buttons
    const pageArrayTmp: Array<number> = [];
    for (let i = 0; i < Math.min(this.totalPage, 5); i++ ) {
      pageArrayTmp.push(this.startPage + i) ;
    }

    this.displayPageNumbers = pageArrayTmp;
  }

  // submit id and status on submit button click
  submitIdStatus(input: any) {
    return this.httpService.post('/api/submit', { 'id': input.id, 'status': input.status}),
    (err: HttpErrorResponse) => {
      console.log (err.message);
    };
  }

}
